package com.Microservico.porta.models;

import javax.persistence.*;

@Entity
@Table(name = "porta")
public class Porta {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name="andar", nullable = false, length = 3, unique = true)
    private long andar;

    @Column(name = "sala", nullable = false, length = 20, unique = true)
    private String sala;

    public Porta() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getAndar() {
        return andar;
    }

    public void setAndar(long andar) {
        this.andar = andar;
    }

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }
}
