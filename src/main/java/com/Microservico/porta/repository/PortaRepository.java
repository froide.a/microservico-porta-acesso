package com.Microservico.porta.repository;

import com.Microservico.porta.models.Porta;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PortaRepository extends JpaRepository<Porta, Long> {
}
