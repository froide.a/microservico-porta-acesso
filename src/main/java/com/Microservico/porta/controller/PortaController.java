package com.Microservico.porta.controller;

import com.Microservico.porta.DTO.CadastroPortaRequest;
import com.Microservico.porta.models.Porta;
import com.Microservico.porta.DTO.PortaResponse;
import com.Microservico.porta.service.PortaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/porta")
public class PortaController {
    @Autowired
    private PortaService portaService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public PortaResponse cadastrarPorta(@RequestBody @Valid CadastroPortaRequest cadastroPortaRequest) {
        try {
            PortaResponse portaResponse = new PortaResponse();
            Porta porta = portaService.cadastrarPorta(
                    cadastroPortaRequest.converterParaPorta(cadastroPortaRequest));
            return portaResponse.converterParaCadastroPortaResponse(porta);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public PortaResponse consultarPortaPorId(@PathVariable(name = "id") long id) {
                   Porta porta = portaService.consultarPortaPorId(id);
            PortaResponse portaResponse = new PortaResponse();
            return portaResponse.converterParaCadastroPortaResponse(porta);

    }
}
