package com.Microservico.porta.DTO;

import com.Microservico.porta.models.Porta;

import javax.persistence.Column;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CadastroPortaRequest {

    @NotNull(message = "Informe o numero do andar.")
    private long andar;

    @NotEmpty(message = "Informe a sala.")
    @Size(max = 20, message = "Maximo permitido é de 20 caracteres")
    private String sala;


    public CadastroPortaRequest() {
    }

    public long getAndar() {
        return andar;
    }

    public void setAndar(long andar) {
        this.andar = andar;
    }

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }

    public Porta converterParaPorta(CadastroPortaRequest cadastroPortaRequest) {
        Porta porta = new Porta();
        porta.setAndar(cadastroPortaRequest.getAndar());
        porta.setSala(cadastroPortaRequest.getSala());

        return porta;
    }

}
