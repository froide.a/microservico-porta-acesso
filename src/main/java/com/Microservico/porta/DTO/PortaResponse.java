package com.Microservico.porta.DTO;

import com.Microservico.porta.models.Porta;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class PortaResponse {

    private long andar;
    private String sala;

    public PortaResponse() {
    }

    public long getAndar() {
        return andar;
    }

    public void setAndar(long andar) {
        this.andar = andar;
    }

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }

    public PortaResponse converterParaCadastroPortaResponse(Porta porta) {
        PortaResponse portaResponse = new PortaResponse();
        portaResponse.setAndar(porta.getAndar());
        portaResponse.setSala(porta.getSala());
        return portaResponse;
    }
}
