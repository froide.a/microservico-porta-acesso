package com.Microservico.porta.Consistencia;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Porta não cadastrada - exception exercicio acesso.")
public class PortaConsistencia extends RuntimeException {

}
