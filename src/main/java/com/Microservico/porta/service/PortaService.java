package com.Microservico.porta.service;

import com.Microservico.porta.Consistencia.PortaConsistencia;
import com.Microservico.porta.models.Porta;
import com.Microservico.porta.repository.PortaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PortaService {
    @Autowired
    private PortaRepository portaRepository;

    public Porta cadastrarPorta(Porta porta) {
        try {
            return portaRepository.save(porta);
        } catch (RuntimeException e) {
            throw new RuntimeException("Erro ao cadastrar a porta: " +
                    e.getMessage(), e);
        }
    }


    public Porta consultarPortaPorId(long id) {

            Optional<Porta> porta = portaRepository.findById(id);
            if (porta.isPresent()) {
                return porta.get();
            } else {
                throw new PortaConsistencia();

            }

    }
}
