package com.Microservico.porta;

import com.MicroservicoPorta.PortaDecoder.PortaDecoder;
import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class PortaConfiguration {

    @Bean
    public ErrorDecoder getPortaDecoder() {
        return new PortaDecoder();
    }

}



