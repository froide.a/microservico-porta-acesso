package com.MicroservicoPorta.PortaDecoder;

import com.Microservico.porta.Consistencia.PortaConsistencia;
import feign.Response;
import feign.codec.ErrorDecoder;

public class PortaDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404) {
            return new PortaConsistencia();
        }
        return errorDecoder.decode(s, response);
    }

}
